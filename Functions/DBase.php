<?php
class DBase
{
    protected $DB;

    function __construct()
    {
        require_once 'DBconn.php';

        $this->DB = new DBconn;
    }

    public function ideaCreate($idea)
    {
        $stmt = $this->DB->conn()->prepare("Insert into idea(title) VALUES (?)");

        if (!$stmt) {
            echo "stmt failed to execute" . "\n";
        } else {
            $stmt->bind_param("s", $idea);
            $stmt->execute();

            echo "stmt executed" . "\n";
        }


    }

    public function showAll()
    {
        $sql = 'SELECT * FROM idea';
        $stmt = $this->DB->conn($sql);
        var_dump($stmt->execute());


    }

}