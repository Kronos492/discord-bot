<?php

use Discord\Parts\User\Game;
use Discord\Discord;

include __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/Functions/Bob.php';
require_once __DIR__.'/Functions/Base.php';

$discord = new \Discord\Discord([
    'token' => 'Token',
]); $discord->on('ready', function ($discord){

    // Discord Presence
    $game = $discord->factory(Game::class, [
        'name' => 'Type -t help',
    ]); $discord->updatePresence($game);
    echo "Online", PHP_EOL;

    // Message array
    $discord->on('message', function ($message, $discord) {
        $guild = $discord->guilds->get('name', 'Guild');
        $var = explode(' ', $message->content);
        echo "{$message->author->username} said: {$message->content}", PHP_EOL;

        if ($var[0] === '-t'){
            $Base = new Base($guild, $var);
            $message->channel->sendmessage($Base->Greet());
        }elseif($var[0] === '-'){
            $response = new response($var);
            $message->channel->sendmessage($response->bGreet());
        };

    });
});
$discord->run();
$discord->updatePresence($game);
